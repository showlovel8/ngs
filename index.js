#!/usr/local/bin/node
var _       = require('underscore');
var net     = require('net');
var zlib    = require('zlib');
var fs      = require('fs');
var yaml    = require('yaml');
var later   = require('later');

var cluster = require('cluster');
var os      = require('os');
var EventEmitter = require('events').EventEmitter;
var events  = new EventEmitter();

var config  = loadConfig();

var redis   = require("redis");
var rclient = redis.createClient();

//==== events listening start ====
events.on('redisData', processRedis);
//==== events listening end ====

//==== data process start ====
function processRedis(err, reply, func, params){
    func(reply, params);
}

function processMax(reply, params){
    if( reply == null || isNaN( Number(reply.split("#")[1]) ) || Number(params.value) > Number(reply.split('#')[1]) ){
        rclient.set( params.mkey, params.time+"#"+params.value );
    }
}

function processAvg(reply, params){
    if( reply == null || isNaN( Number(reply.split("#")[1]) )  ){
        rclient.set( params.akey, params.value );
        rclient.set( params.skey, params.value+"#1" );
    }else{
        var sum   = Number(reply.split('#')[0]);
        var count = Number(reply.split('#')[1]);
        sum      += Number(params.value);
        count ++;
        rclient.set( params.akey, params.time+"#"+sum/count );
        rclient.set( params.skey, sum+"#"+count );
    }
}
//==== data process end ====
console.log('--- Load configs --- ')

rclient.on("error", function (err) {
    console.log("redis client Error " + err);
});

process.on('uncaughtException', function() {
    console.log('progress err: ', arguments );
});

if (cluster.isMaster) {
    // Fork workers.
    var numCPUs = os.cpus().length;
    for (var i = 0; i < numCPUs; i++) {
      cluster.fork();
    }

    //每天下午七点清空数据
    //console.log(config["Redis"]["FlushTime"]);
    var sched = later.parse.cron(config["Redis"]["FlushTime"]);
    later.setInterval(flushredis, sched);

    cluster.on('exit', function(worker, code, signal) {
      console.log('worker ' + worker.process.pid + ' died');
    });

} else {
    var server = net.createServer({ allowHalfOpen: true }, function(c) { //'connection' listener
        c.on('end', function() {});
        c.on('error', function() {
            console.log( 'error from ip:', c.remoteAddress );
            console.log( 'error msg:', arguments );
        }); c.on('data', function(data){ parseData(data);
        });
    	c.end('200 0 0\nOK');
    });

    //console.log(config["Server"]["Port"]);
    //console.log(config["Server"]["IP"]);
    server.listen(config["Server"]["Port"],config["Server"]["IP"],812,function(){});
}

/* ========================================================== */
function loadConfig(){
  var content = fs.readFileSync('etc/config.yaml', 'utf-8');
  var params  = {};
  _.each(yaml.eval(content), function(value, key){
    params[key]=value;
  })
  return  params;
}

function flushredis(){
  console.log("RUN FLUSH DB " + Date());
  rclient.FLUSHDB();
  rclient.end();
}

function parseData( data ){
    var dataStr = data.toString();
    var arr     = dataStr.split('\n');

    if( arr[0].split(' ')[2] == 2 ){
        zlib.gunzip( data.slice( arr[0].length + 1, data.length ), function( err, buf ){
            if (!err) {
                dataStr = buf.toString();
                getCurrent( dataStr );
            }else{
                //console.log( 'header: ', arr[0] );
                //console.log( 'error: ', err, 'data: ', data.toString() );
            }
        });
        // 进入异步处理阶段 后面的无需执行
        return;
    }

    getCurrent( dataStr );
}

function getCurrent( data ){
    _.each( data.split('\n'), function( line, index){
        var tmpArr = line.split('#');
        if( tmpArr.length < 3 ) return;

        var tmpKv = tmpArr.shift().split('=');
        // 不是有效的数据
        if( tmpKv[0] != 'ID' ) return;
        var tmpId = tmpKv[1];

        var tmpKv = tmpArr.shift().split('=');
        // 不是 current 类型的
        if( tmpKv[0] != 'TYPE' || (tmpKv[1] != 'current' && tmpKv[1] != 'day_max') ){ return; }
        var dateType = tmpKv[1];

        var id   = tmpId;
        var time = Date.parse(new Date()) / 1000;

        _.each( tmpArr, function( val, index ){
            // kv 就是 key value 的意思
            var kv    = val.split('=');
            var key   = kv[0];
            var value = kv[1];

            if( typeof value != 'undefined' ){
                value = value.toString().split(':')[1];
            }else{
                value = '';
            }

            if( config["MaxParams"].indexOf(key) > -1 ){
                  var params = new Object();
                  params.mkey  = 'last#'+id+'#'+key+'-day-max';
                  params.akey  = 'last#'+id+'#'+key+'-day-avg';
                  params.skey  = 'sum#'+id+'#'+key;
                  params.time  = time
                  params.value = value

                  rclient.get(params.mkey, function(err, reply){
                    events.emit('redisData', err, reply, processMax, params);
                  });

                  rclient.get(params.skey, function(err, reply){
                    events.emit('redisData', err, reply, processAvg, params);
                  });
             }

             // progress current params
             var rkey     = 'info#'+id+'#'+key;
             var rkeyLast = 'last#'+id+'#'+key;
             var rval     = time+'#'+value;
             rclient.lpush(rkey, rval);
             rclient.setex(rkeyLast, 600, rval, function(){});
             var len = rclient.llen(rkey,function(a,len){
	             if(len>8){ rclient.ltrim(rkey,0,7);}
             });
        });

    });
}
